import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Form} from './form/form.component';
import {ListView} from './listView/list.component';
import { from } from 'rxjs';
const routes: Routes = [
  { path:"addUser" ,component:Form},
  {path : "" , component:ListView},
  {path:"addUser/:id",component:Form}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
